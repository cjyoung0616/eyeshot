﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Navigation;
using System.Windows.Shapes;
using devDept.Eyeshot;
using devDept.Graphics;
using devDept.Eyeshot.Entities;
using devDept.Geometry;
using System.Windows.Threading;
using System.Timers;
using System.Net.Sockets;
using System.Net;
using Microsoft.Win32;
using System.Reflection;
using System.IO;
namespace eyeshotMovingPractice
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    /// 



    public partial class MainWindow : Window
    {

        Dictionary<string, devDept.Eyeshot.Block> blockList;   //cad파일의 블럭화된 모듈을 불러와서 딕셔너리에 대입한다. 
        double currentXPosition = 0;
        double currentYPosition = 0;
        double currentZPosition = 0;

        YonseiEyeshotCnc cncGraphicSimulator;
        double scaleFactor = 2.0;   //eyeshot 좌표와 실제 기계의 이동간의 스케일 팩터

        string zeroPlate = "zeroplate^asmgroup1";           //cad상에 붙여논 블럭 별 이름 (cad와 실제 기계의 차이로 존재하는 블럭 zeroplate)
        string firstPlate = "firstplate^asmgroup";
        string secondPlate = "secondplate^asmgroup";
        string thirdPlate = "thirdplate^asmgroup";
        //string fileDirectory = "C:\\Users\\cms\\Desktop\\finaleyeshotproject\\eyeshotMovingPractice\\asmgroup.STEP";        
        string currentDirectory = null;
        string fileDirectory = null;

        Queue<PointData> pathQueue;
        int curPositionIndex;
        DispatcherTimer AnimationTimer;
        int saveDataIndex;
        int pageDownIndex;

        Socket mySocket;        //HMI와 통신하기 위한 소켓생성

        public MainWindow()
        {
            InitializeComponent();
            currentDirectory = Directory.GetCurrentDirectory();
            fileDirectory = Directory.GetParent(currentDirectory).Parent.FullName+ "\\asmgroup.STEP";

            viewportLayout.Unlock("ULTWPF-9A7X-0X12H-NGEVD-MPLLW");
            pathQueue = new Queue<PointData>();
            AnimationTimer = new DispatcherTimer();
            AnimationTimer.Interval = TimeSpan.FromMilliseconds(10);
            AnimationTimer.Tick += new EventHandler(moveX);
            pageDownIndex = 0;
            makeSocket();
            makeRegistryInformation();
        }

        //레지스트리 정보를 등록 하는 함수
        private void makeRegistryInformation()
        {
            string SchemeName = "com.yonsei.cfrp2";
            //using (var hkcr = Registry.ClassesRoot)
            //{


            //    using (var schemeKey = hkcr.CreateSubKey(SchemeName))
            //    {
            //        //[HKEY_CLASSES_ROOT\com.aruntalkstech.wpftarget]
            //        //@="Url:WPF Target Protocol"
            //        //"URL Protocol"=""
            //        //"UseOriginalUrlEncoding"=dword:00000001
            //        schemeKey.SetValue(string.Empty, "Url: WPF Target Protocol");
            //        schemeKey.SetValue("URL Protocol", string.Empty);
            //        schemeKey.SetValue("UseOriginalUrlEncoding", 1, RegistryValueKind.DWord);

            //        //[HKEY_CLASSES_ROOT\com.aruntalkstech.wpf\shell]
            //        using (var shellKey = schemeKey.CreateSubKey("shell"))
            //        {
            //            //[HKEY_CLASSES_ROOT\com.aruntalkstech.wpf\shell\open]
            //            using (var openKey = shellKey.CreateSubKey("open"))
            //            {
            //                //[HKEY_CLASSES_ROOT\com.aruntalkstech.wpf\shell\open\command]
            //                using (var commandKey = openKey.CreateSubKey("command"))
            //                {
            //                    //@="C:\\github\\SampleCode\\UniversalAppLaunchingWPFApp\\WPFProtocolHandler\\bin\\Debug\\WPFProtocolHandler.exe \"%1\""
            //                    commandKey.SetValue(string.Empty, Assembly.GetExecutingAssembly().Location + " %1");
            //                    commandKey.Close();
            //                }
            //                openKey.Close();
            //            }
            //            shellKey.Close();
            //        }
            //        schemeKey.Close();
            //    }
            //    hkcr.Close();
            //}


            using (var hkcr = Registry.ClassesRoot)
            {



                using (var schemeKey = hkcr.CreateSubKey(SchemeName))
                {
                    //[HKEY_CLASSES_ROOT\com.aruntalkstech.wpftarget]
                    //@="Url:WPF Target Protocol"
                    //"URL Protocol"=""
                    //"UseOriginalUrlEncoding"=dword:00000001
                    schemeKey.SetValue(string.Empty, "Url: WPF Target Protocol");
                    schemeKey.SetValue("URL Protocol", string.Empty);
                    schemeKey.SetValue("UseOriginalUrlEncoding", 1, RegistryValueKind.DWord);

                    //[HKEY_CLASSES_ROOT\com.aruntalkstech.wpf\shell]
                    using (var shellKey = schemeKey.CreateSubKey("shell"))
                    {
                        //[HKEY_CLASSES_ROOT\com.aruntalkstech.wpf\shell\open]
                        using (var openKey = shellKey.CreateSubKey("open"))
                        {
                            //[HKEY_CLASSES_ROOT\com.aruntalkstech.wpf\shell\open\command]
                            using (var commandKey = openKey.CreateSubKey("command"))
                            {
                                //@="C:\\github\\SampleCode\\UniversalAppLaunchingWPFApp\\WPFProtocolHandler\\bin\\Debug\\WPFProtocolHandler.exe \"%1\""
                                commandKey.SetValue(string.Empty, Assembly.GetExecutingAssembly().Location + " %1");
                                commandKey.Close();
                                Console.WriteLine("등록되었습니다.");
                            }
                            openKey.Close();
                        }
                        shellKey.Close();
                    }
                    schemeKey.Close();
                }
                hkcr.Close();
            }


        }



        //소켓을 생성
        public void makeSocket()
        {
            mySocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            //IPAddress ipAddress = GetCurren/tIPAdress();               //소켓 연결이 안될 경우 아래에 있는 식으로 교체한다.
            IPAddress ipAddress = IPAddress.Parse("127.0.0.1");
            if (ipAddress == null)
            {
                Console.WriteLine("socket make wrong");

            }
            IPEndPoint endPoint = new IPEndPoint(ipAddress, 27000);
            mySocket.Bind(endPoint);
            mySocket.Listen(1);
            Console.WriteLine("socket success");

        }

        //내 컴퓨터의 ip를 반환해 준다.
        private static IPAddress GetCurrentIPAdress()
        {
            IPAddress[] addrs = Dns.GetHostEntry(Dns.GetHostName()).AddressList;

            foreach (IPAddress ipAddr in addrs)
            {
                if (ipAddr.AddressFamily == AddressFamily.InterNetwork)
                {
                    Console.WriteLine(ipAddr);
                    return ipAddr;
                }
            }

            return null;
        }

        //cad파일 불러오기
        private void loadCadFile()
        {

            // devDept.Eyeshot.Translators.ReadSTEP step = new devDept.Eyeshot.Translators.ReadSTEP("C:\\Users\\cms\\Desktop\\finaleyeshotproject\\eyeshotMovingPractice\\asmgroup.STEP");
            devDept.Eyeshot.Translators.ReadSTEP step = new devDept.Eyeshot.Translators.ReadSTEP(fileDirectory);
            

            step.Simplify = true;
            viewportLayout.DoWork(step);
            step.LoadingText = "loading";
            blockList = step.Blocks;
            cncGraphicSimulator = new YonseiEyeshotCnc(blockList);
            cncGraphicSimulator.CncFirstPlate = firstPlate;
            cncGraphicSimulator.CncSecondPlate = secondPlate;
            cncGraphicSimulator.CncThirdPlate = thirdPlate;

            //캐드파일 정확히 위치시키기 -> 하드 코딩 나중에 정확히 수정필요
            foreach (var refEntity in blockList[zeroPlate].Entities)
            {

                refEntity.Translate(500, 797, 0);
                refEntity.Rotate(Math.PI / 2, Vector3D.AxisZ);
                refEntity.Translate(-500, -797, 0);

            }

            foreach (var refEntity in blockList[firstPlate].Entities)
            {


                refEntity.Translate(500, 797, 0);
                refEntity.Rotate(-Math.PI / 2, Vector3D.AxisZ);
                refEntity.Translate(-500, -797, 0);
            }
            foreach (var refEntity in blockList[secondPlate].Entities)
            {
                refEntity.Translate(500, 797, 0);
                refEntity.Rotate(-Math.PI / 2, Vector3D.AxisZ);
                refEntity.Translate(-650, -797, 0);
            }

            step.AddToScene(viewportLayout);
        }


        //윈도우가 생성될 때 윈도우창에 띄우는 것들을 생성.
        protected override void OnContentRendered(EventArgs e)
        {
            loadCadFile();

            //viewportlayout 각종 설정, 그림자 등..;;
            viewportLayout.Rendered.SilhouettesDrawingMode = silhouettesDrawingType.Never;
            viewportLayout.Rendered.ShadowMode = shadowType.None;
            viewportLayout.Rendered.EdgeColorMethod = edgeColorMethodType.SingleColor;
            viewportLayout.DisplayMode = displayType.Rendered;
            Console.WriteLine(viewportLayout.IsHardwareAccelerated);
            viewportLayout.ZoomFit();
            viewportLayout.Invalidate();
            Thread serverThread = new Thread(receiveData);    //tcp 소켓 통신을 위한 쓰레드 생성
            serverThread.Start();


            base.OnContentRendered(e);
        }



        public void receiveData()
        {
            Console.WriteLine("thread run");
            Socket clntSocket = mySocket.Accept();

            while (true)
            {
                Console.WriteLine("whatwhat");
                byte[] recvBytes = new byte[24];
                clntSocket.Receive(recvBytes);

                double xPosition = 0, yPosition = 0, zPosition = 0;

                xPosition = BitConverter.ToDouble(recvBytes, 0);
                yPosition = BitConverter.ToDouble(recvBytes, 8);
                zPosition = BitConverter.ToDouble(recvBytes, 16);
                Console.WriteLine(xPosition);
                Console.WriteLine(yPosition);
                Console.WriteLine(zPosition);

                xPosition *= scaleFactor;          //eyeshot 좌표와 실제 기계의 이동간의 스케일 팩터
                yPosition *= scaleFactor;
                zPosition *= scaleFactor;

                cncGraphicSimulator.moveXAxis(xPosition);
                cncGraphicSimulator.moveYAxis(yPosition);
                cncGraphicSimulator.moveZAxis(zPosition);



                currentXPosition = xPosition;
                currentYPosition = yPosition;
                currentZPosition = zPosition;



                viewportLayout.Entities.Regen();

                viewportLayout.Invalidate();


                curPositionIndex++;
                Dispatcher.Invoke(new Action(() => this.ScrollRun()));   // 현재 위치를 찍어주는 기능, 스크롤뷰에도 찍어야 할까? 패스가 유실되므로 필요없을듯..;;

                pageDownIndex++;
            }

            clntSocket.Close();
        }

        public void ScrollRun()
        {


            textblock.Text = Convert.ToString(currentXPosition) + ", " + Convert.ToString(currentYPosition) + ", " + Convert.ToString(currentZPosition);

        }

        private void xclick(object sender, RoutedEventArgs e)
        {

            currentXPosition += 10;


            cncGraphicSimulator.moveXAxis(cncGraphicSimulator.CncXPosition + 10);

            textblock.Text = Convert.ToString(currentXPosition) + ", " + Convert.ToString(currentYPosition) + ", " + Convert.ToString(currentZPosition);
            Console.WriteLine("click");
            viewportLayout.Entities.Regen();

            viewportLayout.Invalidate();
        }

        private void yclick(object sender, RoutedEventArgs e)
        {

            cncGraphicSimulator.moveYAxis(cncGraphicSimulator.CncYPosition + 10);


            currentYPosition += 10;
            textblock.Text = Convert.ToString(currentXPosition) + ", " + Convert.ToString(currentYPosition) + ", " + Convert.ToString(currentZPosition);

            viewportLayout.Entities.Regen();
            viewportLayout.Invalidate();

        }

        private void zclick(object sender, RoutedEventArgs e)
        {

            cncGraphicSimulator.moveZAxis(cncGraphicSimulator.CncZPosition + 10);


            currentZPosition += 10;
            textblock.Text = Convert.ToString(currentXPosition) + ", " + Convert.ToString(currentYPosition) + ", " + Convert.ToString(currentZPosition);

            viewportLayout.Entities.Regen();

            viewportLayout.Invalidate();

        }

        private void minusXClick(object sender, RoutedEventArgs e)
        {

            cncGraphicSimulator.moveXAxis(cncGraphicSimulator.CncXPosition - 10);


            currentXPosition -= 10;
            textblock.Text = Convert.ToString(currentXPosition) + ", " + Convert.ToString(currentYPosition) + ", " + Convert.ToString(currentZPosition);

            viewportLayout.Entities.Regen();
            viewportLayout.Invalidate();
        }

        private void minusYClick(object sender, RoutedEventArgs e)
        {

            cncGraphicSimulator.moveYAxis(cncGraphicSimulator.CncYPosition - 10);

            currentYPosition -= 10;
            textblock.Text = Convert.ToString(currentXPosition) + ", " + Convert.ToString(currentYPosition) + ", " + Convert.ToString(currentZPosition);

            viewportLayout.Entities.Regen();

            viewportLayout.Invalidate();

        }

        private void minusZClick(object sender, RoutedEventArgs e)
        {

            cncGraphicSimulator.moveZAxis(cncGraphicSimulator.CncZPosition - 10);
            currentZPosition -= 10;
            textblock.Text = Convert.ToString(currentXPosition) + ", " + Convert.ToString(currentYPosition) + ", " + Convert.ToString(currentZPosition);
            viewportLayout.Entities.Regen();
            viewportLayout.Invalidate();
        }

        private void moveClick(object sender, RoutedEventArgs e)
        {
            string str = textbox1.Text;

            if (str == string.Empty)
                return;

            string[] strArray;
            strArray = str.Split(',');


            double xPosition = currentXPosition;
            double yPosition = currentYPosition;
            double zPosition = currentZPosition;
            bool isDoubleX = Double.TryParse(strArray[0], out xPosition);
            bool isDoubleY = false;
            bool isDoubleZ = false;
            if (strArray.Length >= 2)
                isDoubleY = Double.TryParse(strArray[1], out yPosition);
            if (strArray.Length >= 3)
                isDoubleZ = Double.TryParse(strArray[2], out zPosition);
            if ((isDoubleX == false) || (isDoubleY == false) || (isDoubleZ == false))
            {
                return;
            }


            cncGraphicSimulator.moveXAxis(xPosition);
            cncGraphicSimulator.moveYAxis(yPosition);
            cncGraphicSimulator.moveZAxis(zPosition);
            currentXPosition = xPosition;
            currentYPosition = yPosition;
            currentZPosition = zPosition;
            textblock.Text = Convert.ToString(currentXPosition) + ", " + Convert.ToString(currentYPosition) + ", " + Convert.ToString(currentZPosition - 30);

            viewportLayout.Entities.Regen();

            viewportLayout.Invalidate();

        }

        private void WindowOnTextBoxFocus(object sender, RoutedEventArgs e)
        {
            textbox1.Text = "";
        }

        private void AnimateCnc(object sender, RoutedEventArgs e)
        {
            AnimationTimer.Start();

            saveDataIndex = 0;

        }
        private void moveX(object sender, EventArgs e)
        {

            if (curPositionIndex >= pathQueue.Count)
            {

                AnimationTimer.Stop();
                positionScrollViewer.ScrollToTop();
                curPositionIndex = 0;
                pageDownIndex = 0;

                if (pathQueue.Count <= 0)
                    return;

                TextBlock changeBlcok = scrollLayoutPanel.Children[pathQueue.Count - 1] as TextBlock;
                changeBlcok.Foreground = Brushes.White;

                return;
            }


            double xPosition = (pathQueue.ElementAt<PointData>(curPositionIndex).xCncPosition);
            double yPosition = (pathQueue.ElementAt<PointData>(curPositionIndex).yCncPosition);
            double zPosition = (pathQueue.ElementAt<PointData>(curPositionIndex).zCncPosition);

            cncGraphicSimulator.moveXAxis(xPosition);
            cncGraphicSimulator.moveYAxis(yPosition);
            cncGraphicSimulator.moveZAxis(zPosition);

            currentXPosition = xPosition;
            currentYPosition = yPosition;
            currentZPosition = zPosition;
            textblock.Text = Convert.ToString(currentXPosition) + ", " + Convert.ToString(currentYPosition) + ", " + Convert.ToString(currentZPosition);
            viewportLayout.Entities.Regen();

            viewportLayout.Invalidate();

            TextBlock refBlock;
            if (curPositionIndex == 0)
            {
                refBlock = scrollLayoutPanel.Children[curPositionIndex] as TextBlock;
                refBlock.Background = Brushes.Red;
            }
            else
            {
                refBlock = scrollLayoutPanel.Children[curPositionIndex - 1] as TextBlock;
                refBlock.Background = Brushes.White;
                refBlock = scrollLayoutPanel.Children[curPositionIndex] as TextBlock;
                refBlock.Background = Brushes.Red;
                if (positionScrollViewer.ActualHeight <= refBlock.ActualHeight * (pageDownIndex + 1))
                {
                    positionScrollViewer.PageDown();
                    pageDownIndex = -1;
                }

            }


            curPositionIndex++;
            pageDownIndex++;
            //   Console.WriteLine("hello" + curPositionIndex.ToString()); 

        }

        private void SelectFile(object sender, RoutedEventArgs e)
        {

            if (pathQueue.Count > 0)
            {
                pathQueue.Clear();
                scrollLayoutPanel.Children.Clear();
            }



            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            dlg.DefaultExt = ".txt";
            dlg.Filter = "txt Files (*.txt)|*.txt";


            Nullable<bool> result = dlg.ShowDialog();


            if (result == true)
            {
                // Open document 
                string filename = dlg.FileName;
                System.IO.StreamReader sr = new System.IO.StreamReader(filename);
                saveDataIndex = 0;
                int saveNumber = 0;
                while (!sr.EndOfStream)
                {
                    string change = sr.ReadLine();

                    SaveCncPath(change);
                    saveNumber++;
                    saveDataIndex++;

                    /*
                    if (saveDataIndex%40 == 0)
                    {
                        SaveCncPath(change);
                        saveNumber++;
                        saveDataIndex++;
                    }
                    
                    else
                        saveDataIndex++;
                    */
                }

                sr.Close();

                Console.WriteLine(pathQueue.Count);
                Console.WriteLine(saveNumber);
                Console.WriteLine(saveNumber);

                MakePosiionScrollView();

            }

        }

        private void SaveCncPath(string onePosition)
        {
            string[] strRef = new string[3];
            strRef = onePosition.Split('\t');
            Console.WriteLine(strRef.Length);
            PointData curPosition;
            curPosition.xCncPosition = Convert.ToDouble(strRef[0]);
            if (strRef.Length < 2)
                curPosition.yCncPosition = 0;
            else
                curPosition.yCncPosition = Convert.ToDouble(strRef[1]);

            if (strRef.Length < 3)
                curPosition.zCncPosition = 0;
            else
                curPosition.zCncPosition = Convert.ToDouble(strRef[2]);

            pathQueue.Enqueue(curPosition);
        }
        private void MakePosiionScrollView()
        {

            foreach (var iter in pathQueue)
            {
                TextBlock myBlock = new TextBlock();

                myBlock.Text = "x:" + Math.Round(iter.xCncPosition, 3).ToString() + " y:" + Math.Round(iter.yCncPosition, 3).ToString() + " z:" + Math.Round(iter.zCncPosition, 3).ToString();
                scrollLayoutPanel.Children.Add(myBlock);

            }

        }


        private void originClick(object sender, RoutedEventArgs e)
        {
            currentXPosition = 0;
            currentYPosition = 0;
            currentZPosition = 0;
            cncGraphicSimulator.CncXPosition = 0;
            cncGraphicSimulator.CncYPosition = 0;
            cncGraphicSimulator.CncZPosition = 0;

        }



        private void scrollTouchDown(object sender, MouseButtonEventArgs e)
        {
            TextBlock _canvas = sender as TextBlock;
            Console.WriteLine("touched");
        }

    }




}
