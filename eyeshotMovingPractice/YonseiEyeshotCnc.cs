﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using devDept.Eyeshot;
using devDept.Eyeshot.Entities;

namespace eyeshotMovingPractice
{

    struct PointData
    {
        public double xCncPosition;
        public double yCncPosition;
        public double zCncPosition;
    }

    class YonseiEyeshotCnc
    {
        PointData mCncPosition;  //cnc의 멤버변수는 좌표값과 그 좌표값에 해당하는 
        devDept.Eyeshot.Block[] mBlockList;

        string firstPlate = "firstplate^asmgroup";              //cad상에 붙여논 블럭 별 이름
        string secondPlate = "secondplate^asmgroup";
        string thirdPlate = "thirdplate^asmgroup";

        // cnc의 좌표정보의 프로퍼티
        public PointData CncPosition
        {
            set { mCncPosition = value; }
            get { return mCncPosition;  }
        }

        public double CncXPosition
        {
            set { mCncPosition.xCncPosition = value; }
            get { return mCncPosition.xCncPosition; }
        }
        public double CncYPosition
        {
            set { mCncPosition.yCncPosition = value; }
            get { return mCncPosition.yCncPosition; }
        }
        public double CncZPosition
        {
            set { mCncPosition.zCncPosition = value; }
            get { return mCncPosition.zCncPosition; }
        }
        // cnc 파일 블럭의 프로퍼티
        public string CncFirstPlate
        {
            set { firstPlate = value; }
            get { return firstPlate;  }
        }
        public string CncSecondPlate
        {
            set { secondPlate = value; }
            get { return secondPlate; }
        }
        public string CncThirdPlate
        {
            set { thirdPlate = value; }
            get { return thirdPlate; }
        }

        public YonseiEyeshotCnc(Dictionary<string, devDept.Eyeshot.Block> list)
        {
            mBlockList = new devDept.Eyeshot.Block[3];
            mBlockList[0] = list[firstPlate];
            mBlockList[1] = list[secondPlate];
            mBlockList[2] = list[thirdPlate];
            mCncPosition.xCncPosition = 0;
            mCncPosition.yCncPosition = 0;
            mCncPosition.zCncPosition = 0;

        }

        public void moveXAxis(double xPosition)
        {
            double deltaX = xPosition - mCncPosition.xCncPosition;
            foreach (var refEntity in mBlockList[1].Entities)
            {
                refEntity.Translate(deltaX, 0, 0);
            }
            mCncPosition.xCncPosition += deltaX;

        }
        public void moveYAxis(double yPosition)
        {
            double deltaY = yPosition - mCncPosition.yCncPosition;

            foreach (var refEntity in mBlockList[0].Entities)
            {
                refEntity.Translate(0, deltaY, 0);
            }
            foreach (var refEntity in mBlockList[1].Entities)
            {
                refEntity.Translate(0, deltaY, 0);
            }
            mCncPosition.yCncPosition += deltaY;

        }
        public void moveZAxis(double zPosition)
        {
            double deltaZ = zPosition - mCncPosition.zCncPosition;

            foreach (var refEntity in mBlockList[2].Entities)
            {
                refEntity.Translate(0, 0, deltaZ);
            }
            mCncPosition.zCncPosition += deltaZ;
        }

    }
}
